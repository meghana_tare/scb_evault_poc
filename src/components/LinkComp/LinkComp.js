import React from 'react';
import { IoEyeOutline } from 'react-icons/io5';
import { VscEdit, VscDiffAdded, VscTrash } from 'react-icons/vsc';
import './LinkComp.css'

class LinkComp extends React.Component {
    render() {
        if (this.props.colDef.headerName === 'View Client') {
            return (
                <div className="algn" >
                    <a href="./createClient"><IoEyeOutline /></a>
                </div>
            )
        } else if (this.props.colDef.headerName === 'Edit Client') {
            return (
                <div className="algn" >
                    <a href="./createClient"><VscEdit /></a>
                </div>
            )
        } else if (this.props.colDef.headerName === 'Add Tree') {
            return (
                <div className="algn" >
                    <a href="./createTree"><VscDiffAdded /></a>
                </div>
            )
        } else if (this.props.colDef.headerName === 'Delete Client') {
            return (
                <div className="algn" >
                    <a href="./"><VscTrash /></a>
                </div>
            )
        }
    }
}

export default LinkComp
