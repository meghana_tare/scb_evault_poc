import React from 'react';
import { IoEyeOutline } from 'react-icons/io5';
import { VscEdit, VscDiffAdded, VscTrash } from 'react-icons/vsc';
import './TreeLinks.css';

class TreeLinks extends React.Component {
    render() {
        if (this.props.colDef.headerName === 'View Tree') {
            return (
                <div className="algn" >
                    <a href="./createTree"><IoEyeOutline /></a>
                </div>
            )
        } else if (this.props.colDef.headerName === 'Edit Tree') {
            return (
                <div className="algn" >
                    <a href="./createTree"><VscEdit /></a>
                </div>
            )
        } else if (this.props.colDef.headerName === 'Add Tree') {
            return (
                <div className="algn" >
                    <a href="./createTree"><VscDiffAdded /></a>
                </div>
            )
        } else if (this.props.colDef.headerName === 'Delete Tree') {
            return (
                <div className="algn" >
                    <a href="./"><VscTrash /></a>
                </div>
            )
        }
    }
}

export default TreeLinks
