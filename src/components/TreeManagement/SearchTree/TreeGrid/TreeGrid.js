import React from 'react';
import { AgGridReact, AgGridColumn } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';
import './TreeGrid.css';
import TreeLinks from '../TreeLinks/TreeLinks';

class TreeGrid extends React.Component {
    state = {
        rowData: [
            { TreeId: "123", ClientId: "4353", VaultId: "1321", Parties: "Party1", TopUp: 1000 },
            { TreeId: "456", ClientId: "2748", VaultId: "1321", Parties: "Party1", TopUp: 1000 },
            { TreeId: "789", ClientId: "9379", VaultId: "1321", Parties: "Party1", TopUp: 1000 },
            { TreeId: "123", ClientId: "4353", VaultId: "1321", Parties: "Party1", TopUp: 1000 },
            { TreeId: "456", ClientId: "2748", VaultId: "1321", Parties: "Party1", TopUp: 1000 },
            { TreeId: "789", ClientId: "9379", VaultId: "1321", Parties: "Party1", TopUp: 1000 },
            { TreeId: "123", ClientId: "4353", VaultId: "1321", Parties: "Party1", TopUp: 1000 },
            { TreeId: "456", ClientId: "2748", VaultId: "1321", Parties: "Party1", TopUp: 1000 },
            { TreeId: "789", ClientId: "9379", VaultId: "1321", Parties: "Party1", TopUp: 1000 }
        ]
    }

    render() {

        return (
            <>
                <div className="ag-theme-balham grid" >
                    <AgGridReact
                        rowData={this.state.rowData}>
                        <AgGridColumn headerName="Tree Id" field="TreeId" sortable={true} filter={true}></AgGridColumn>
                        <AgGridColumn headerName="Client Id" field="ClientId" sortable={true} filter={true}></AgGridColumn>
                        <AgGridColumn headerName="Vault Id" field="VaultId" sortable={true} filter={true}></AgGridColumn>
                        <AgGridColumn headerName="Parties" field="Parties" sortable={true} filter={true}></AgGridColumn>
                        <AgGridColumn headerName="Top Up" field="TopUp" sortable={true} filter={true}></AgGridColumn>
                        <AgGridColumn headerName="Edit Tree" width={120} field="EditTree" sortable={true} filter={true}
                            cellRendererFramework={TreeLinks}></AgGridColumn>
                        <AgGridColumn headerName="View Tree" width={120} field="ViewTree" sortable={true} filter={true}
                            cellRendererFramework={TreeLinks}></AgGridColumn>
                        <AgGridColumn headerName="Delete Tree" width={120} field="DeleteTree" sortable={true} filter={true}
                            cellRendererFramework={TreeLinks}></AgGridColumn>
                    </AgGridReact >
                </div >
            </>
        )
    }
}

export default TreeGrid
