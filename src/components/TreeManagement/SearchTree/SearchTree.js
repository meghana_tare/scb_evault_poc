import React from 'react';
import { Button, Row, Col } from 'react-bootstrap';
import TreeGrid from './TreeGrid/TreeGrid';
import { VscRefresh, VscSearch } from "react-icons/vsc";
import './SearchTree.css';

class SearchTree extends React.Component {
    state = {
        searchText: ''
    }

    onSearchClick(e) {
        this.setState({ searchText: '' })
    }

    onResetClick(e) {
        this.setState({ searchText: '' })
    }

    render() {
        return (
            <div className="divStyle">
                <Row>
                    <Col>
                        <h2>Search Tree</h2>
                    </Col>
                </Row>
                <Row>
                    <Col className="searchStyle" md={{ span: 3, offset: 8 }}>
                        <input
                            className={"searchStyle h-100"}
                            key="random1"
                            value={this.state.searchText}
                            placeholder={"Search Tree"}
                            onChange={(e) => this.setState({ searchText: e.target.value })}
                        />
                    </Col>
                    <Button onClick={(e) => { this.onSearchClick(e) }}><VscSearch /></Button>
                    <Button className="resetBtn" onClick={(e) => { this.onResetClick(e) }}><VscRefresh /></Button>
                </Row>
                <br />
                <Row>
                    <Col md="12">
                       <TreeGrid></TreeGrid>
                    </Col>
                </Row>
            </div >
        )
    }
}

export default SearchTree
