let aliasArr = {
	'CREATE VAULT': {
		name: 'CREATE VAULT',
		lable: ['SUCCESS', 'DUPLICATE', 'TRY AGAIN', 'TECH. ERROR'],
		alias: ['TOP UP', 'INFORM CLIENT', 'CREATE VAULT', 'INFORM CLIENT']
	},
	'TOP UP': {
		name: 'TOP UP',
		lable: ['SUCCESS', 'DUPLICATE', 'TRY AGAIN', 'TECH. ERROR'],
		alias: ['CREATE PARTY', 'INFORM CLIENT', 'TOP UP', 'INFORM CLIENT']
	},
	'INFORM CLIENT': {
		name: 'INFORM CLIENT',
		lable: ['SUCCESS', 'SERVER ERROR', 'TRY AGAIN', 'TECH. ERROR'],
		alias: ['SUCCESS', 'INFORM CLIENT', 'INFORM CLIENT', 'INFORM CLIENT']
	},
	'CREATE PARTY': {
		name: 'CREATE PARTY',
		lable: ['SUCCESS', 'SERVER ERROR', 'TRY AGAIN', 'TECH. ERROR'],
		alias: ['SUCCESS', 'INFORM CLIENT', 'CREATE PARTY', 'INFORM CLIENT']
	}
};

export default aliasArr;