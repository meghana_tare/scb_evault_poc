import React from 'react'
import Tree from 'react-d3-tree';
import './TreeComponent.css';
import Lego from './Lego'

class TreeComponent extends React.Component {
    data = this.props.nodeDatum;

    renderForeignObjectNode = ({
        nodeDatum,
        toggleNode,
        foreignObjectProps
    }) => (
        <g>
            <foreignObject {...foreignObjectProps}>
                <Lego nodeDatum={nodeDatum.name}></Lego>
            </foreignObject>
        </g>
    )

    render() {
        const nodeSize = { x: 400, y: 300 };
        const foreignObjectProps = { width: nodeSize.x, height: nodeSize.y, x: -200, y: -200 };

        return (
            <div id="treeWrapper" style={{ width: '100%', height: '100%' }}>
                <Tree data={this.data}
                    pathFunc="step"
                    collapsible={false}
                    orientation="vertical"
                    depthFactor="-350"
                    translate={{ x: "600", y: "600" }}
                    separation={{ siblings: 2, nonSiblings: 3 }}
                    // nodeSize={{x: '200', y: '700' }}
                    renderCustomNodeElement={(rd3tProps) =>
                        this.renderForeignObjectNode({ ...rd3tProps, foreignObjectProps })
                    }
                />
            </div>
        )
    }
}

export default TreeComponent
