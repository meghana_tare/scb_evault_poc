import React from 'react';
import './Lego.css'

class Lego extends React.Component {
    getSide1Class(name) {
        if (this.props.bgColor === 'red') {
            return ' ' + name + 'red';
        } else if (this.props.bgColor === undefined) {
            return ' side1white';
        } else {
            return ''
        }
    }

    render() {
        return (
            <div>
                <div className={'side1' + this.getSide1Class('side1')}>{this.props.nodeDatum}</div>
                <div className={'side2' + this.getSide1Class('side2')}></div>
                <div className='side3-bin'>
                    <div className={'side3' + this.getSide1Class('side3')} style={{ left: this.props.left, top: this.props.top ? this.props.top : '27px' }}></div>
                </div >
                <div className={'stud1' + this.getSide1Class('stud1')}></div>
                <div className={'stud2' + this.getSide1Class('stud2')}></div>
                <div className={'stud3' + this.getSide1Class('stud3')}></div>
                <div className={'stud4' + this.getSide1Class('stud4')}></div>
                <div className={'stud5' + this.getSide1Class('stud5')}></div>
                <div className={'stud6' + this.getSide1Class('stud6')}></div>
                <div className={'stud7' + this.getSide1Class('stud7')}></div>
                <div className={'stud8' + this.getSide1Class('stud8')}></div>
            </div >
        )
    }
}

export default Lego
