import React from 'react';
import { Row, Col } from "react-bootstrap";
import { ToastContainer } from "react-toastify";
import TabArea1 from './TabArea/TabArea1';
import ListArea from './ListArea/ListArea';

class CreateTree extends React.Component{
    state = {
        arrayItems: ['CREATE VAULT', 'TOP UP', 'CREATE PARTY', 'INFORM CLIENT']
    }

    render() {
        return (
            <div className="h-100" >
                <ToastContainer position="top-center" />
                <Row className="h-100" noGutters>
                    <Col className="h-100" sm={10}>
                        <TabArea1></TabArea1>
                    </Col>
                    <Col className="h-100" sm={2}>
                        <ListArea array={this.state.arrayItems}></ListArea>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default CreateTree
