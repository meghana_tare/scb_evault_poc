import React from 'react';
import Tree from 'react-d3-tree';
import Lego from '../D3Tree/Lego'
import aliasArr from '../Data';
import Modal from "react-bootstrap/Modal";
import { Col, Form, Button, Tab, Tabs } from 'react-bootstrap';
import './TabArea1.css';
import { VscArrowLeft } from "react-icons/vsc";
import ReactTooltip from "react-tooltip";
import { Redirect } from 'react-router-dom';

class TabArea1 extends React.Component {
    state = {
        count: 0,
        show: false,
        title: '',
        checkboxValues: { id: '' },
        className: ''
    }
    arrayItems = ['CREATE VAULT', 'TOP UP', 'CREATE PARTY', 'INFORM CLIENT'];
    counter = 0;
    data = [];
    zoom = false;
    zoomIds = [];
    hvrZoomIds = [];

    drop = (ev) => {
        const data = ev.dataTransfer.getData("text");
        this.zoomIds = [];
        if (data === ev.target.getAttribute('aliasname')) {
            ev.preventDefault()
        } else {
            return false
        }
        this.pushElement(data, ev.target.id);
    }

    allowDrop = (e) => {
        e.preventDefault();
    }

    pushElement = (data, id) => {
        if (this.data.length === 0) {
            const parentId = this.getId();
            this.data = [
                {
                    id: parentId, name: 'CREATE VAULT', parent: null, bgColor: 'blue', alias: 'CREATE VAULT', zoom: true, children: [
                        { id: this.getId(), name: 'SUCCESS', parent: parentId, alias: this.getAliasName('CREATE VAULT', 0), zoom: false, children: [] },
                        { id: this.getId(), name: 'DUPLICATE', parent: parentId, alias: this.getAliasName('CREATE VAULT', 1), zoom: false, children: [] },
                        { id: this.getId(), name: 'TRY AGAIN', parent: parentId, alias: this.getAliasName('CREATE VAULT', 2), zoom: false, children: [] },
                        { id: this.getId(), name: 'TECH. ERROR', parent: parentId, alias: this.getAliasName('CREATE VAULT', 3), zoom: false, children: [] }
                    ]
                }
            ];
            this.pushArr(this.data[0].children, id, this.data[0].alias)
        } else {
            this.pushArr(this.data[0].children, id, data);
        }
        this.getTree();
    }

    getId = () => {
        this.counter++
        return this.counter;
    }

    getAliasName = (alias, idx) => {
        return aliasArr[alias].alias[idx];
    }

    pushArr = (arr, id, alias) => {
        arr.forEach((element, index) => {
            if (element.id === Number(id)) {
                element.name = element.alias;
                element.bgColor = 'blue';
                element.children.push(
                    { id: this.getId(), name: this.getNodeLable(alias, 0), parent: element.id, zoom: false, alias: this.getAliasName(alias, 0), children: [] },
                    { id: this.getId(), name: this.getNodeLable(alias, 1), parent: element.id, zoom: false, alias: this.getAliasName(alias, 1), children: [] },
                    { id: this.getId(), name: this.getNodeLable(alias, 2), parent: element.id, zoom: false, alias: this.getAliasName(alias, 2), children: [] },
                    { id: this.getId(), name: this.getNodeLable(alias, 3), parent: element.id, zoom: false, alias: this.getAliasName(alias, 3), children: [] }
                )
                this.getZoomIds(this.data, id);
                this.zoomInOut(this.data, id);
            } else {
                this.pushArr(element.children, id, alias);
            }
        });
        console.log(this.data)
    }

    getNodeLable = (alias, idx) => {
        return aliasArr[alias].lable[idx];
    }

    getZoomIds = (arr, id) => {
        arr.forEach(elem => {
            if (elem.id === +id) {
                this.zoomIds.push(+id);
                this.getZoomIds(this.data, elem.parent);
            } else {
                if (elem.children.length) {
                    this.getZoomIds(elem.children, id);
                }
            }
        });
    }

    zoomInOut = (arr) => {
        arr.forEach(elem => {
            if (this.zoomIds.includes(elem.id)) {
                elem.zoom = true;
                this.zoomInOut(elem.children);
            } else {
                elem.zoom = false;
                this.zoomInOut(elem.children);
            }
        });
    }

    onNodeClicked = () => {
        this.data[0].children.pop();
        this.getTree();
    }

    getTree = () => {
        this.data = [this.data[0]];
        this.setState({ count: this.state.count + 1 });
    }

    renderForeignObjectNode = ({
        nodeDatum,
        foreignObjectProps
    }) => (
        < g onMouseOver={(e) => { this.onMousehvr(nodeDatum.id) }} onMouseOut={(e) => { this.onMouseOut(this.data) }}>
            <foreignObject {...foreignObjectProps} style={{ transform: nodeDatum.zoom || nodeDatum.className ? 'scale(1)' : 'scale(0.60)' }}>
                <div style={{
                    display: 'inline-block', width: '300px', height: '300px', left: '50px', fontWeight: 'bold',
                    whiteSpace: 'nowrap', fontSize: nodeDatum.zoom ? 'medium' : 'xx-small',
                    color: 'black', zIndex: '999', position: 'absolute'
                }} id={nodeDatum.id} aliasname={nodeDatum.alias} onDrop={this.drop} onDragOver={this.allowDrop}
                    onClick={(e) => { this.handleShow(e, nodeDatum) }}>
                </div>
                <div
                    style={{
                        display: 'inline-block', fontWeight: 'bold',
                        whiteSpace: 'nowrap', fontSize: nodeDatum.zoom ? 'medium' : 'xx-small',
                        color: 'black'
                    }}>
                    <h3 style={{ textAlign: "center" }}>
                        <Lego drop-disable zoom={nodeDatum.zoom} bgColor={nodeDatum.bgColor} nodeDatum={nodeDatum.name} left={nodeDatum.name.includes(this.arrayItems) ? '-88px' : '-167px'} >
                        </Lego>
                    </h3>
                </div>
            </foreignObject>
        </g>
    )

    onMousehvr(id) {
        debugger
        this.getIds(this.data, id);
        this.zoomHvrIds(this.data);
        this.getTree();
    }

    getIds = (arr, id) => {
        arr.forEach(elem => {
            if (elem.id === +id) {
                this.hvrZoomIds.push(+id);
                if (elem.parent) {
                    this.getIds(this.data, elem.parent);
                }
            } else {
                if (elem.children.length) {
                    this.getIds(elem.children, id);
                }
            }
        });
    }

    zoomHvrIds = (arr) => {
        arr.forEach(elem => {
            if (this.hvrZoomIds.includes(elem.id)) {
                elem.className = 'onhvr';
                this.zoomHvrIds(elem.children);
            } else {
                elem.className = '';
                this.zoomHvrIds(elem.children);
            }
        });
    }

    onMouseOut(arr) {
        this.hvrZoomIds = [];
        arr.forEach(elem => {
            elem.className = '';
            if (elem.children.length) {
                this.zoomHvrIds(elem.children);
            }
        });
        this.getTree();
    }

    handleShow(e, nodeDatum) {
        if (nodeDatum.children.length) {
            this.setState({ title: e.target.getAttribute('aliasname'), show: true, popupId: nodeDatum.id });
        }
    }

    handleClose(e) {
        this.setState({ show: false });
    }

    onCurrentPathSet(event) {
        const newCheckboxValues = this.state.checkboxValues;
        newCheckboxValues[this.state.popupId] = event.target.checked
        this.setState({ checkboxValues: newCheckboxValues });
        if (this.state.checkboxValues[this.state.popupId]) {
            this.getCurrentPath(this.data[0].children, 'red');
        } else {
            this.getCurrentPath(this.data[0].children, 'blue');
        }
    }

    getCurrentPath(arr, color) {
        if (this.data[0].id === this.state.popupId) {
            this.data[0].bgColor = color;
            this.getTree();
        } else {
            arr.forEach((element, index) => {
                if (element.id === this.state.popupId) {
                    element.bgColor = color;
                    this.getTree();
                } else {
                    if (element.children.length) {
                        this.getCurrentPath(element.children, color);
                    }
                }
            });
        }
    }

    onBackClick(e) {
        return <Redirect to="/" />
    }

    render() {
        const nodeSize = { x: 350, y: 300 };
        const foreignObjectProps = { width: nodeSize.x, height: nodeSize.y, x: -200, y: -200 };

        return (
            <>
                <div className="dragArea">
                    <Button className="btnStyl" variant="outline-primary" data-tip="Back" onClick={(e) => document.location.href = '/'}>
                        <VscArrowLeft />
                        <ReactTooltip type="light" />
                    </Button>
                    <Button className="btnStyl2" variant="primary" onClick={(e) => document.location.href = '/'}>
                        Save</Button>
                    {!this.data.length ? <div className="dragOver" aliasname="CREATE VAULT" id="1" onDrop={(e) => { this.drop(e) }} onDragOver={(e) => { this.allowDrop(e) }}>
                        Create Vault</div> : null}
                    <div className="h-100">
                        {this.data.length ? <Tree data={this.data}
                            pathFunc="step"
                            onNodeClick={() => this.onNodeClicked()}
                            collapsible={false}
                            orientation="vertical"
                            depthFactor="-350"
                            translate={{ x: "600", y: "590" }}
                            separation={{ siblings: 2, nonSiblings: 3 }}
                            renderCustomNodeElement={(rd3tProps) =>
                                this.renderForeignObjectNode({ ...rd3tProps, foreignObjectProps })
                            }
                        /> : null}

                        <Modal centered show={this.state.show} onHide={(e) => { this.handleClose(e) }}>
                            <Modal.Header closeButton>
                                <Modal.Title>{this.state.title}</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                                <Tabs defaultActiveKey="basic" id="uncontrolled-tab-example" className="mb-3">
                                    <Tab eventKey="basic" title="Basic Info">
                                        <p>
                                            Ipsum molestiae natus adipisci modi eligendi? Debitis amet quae unde
                                            commodi aspernatur enim, consectetur. Cumque deleniti temporibus
                                            ipsam atque a dolores quisquam quisquam adipisci possimus
                                            laboriosam. Quibusdam facilis doloribus debitis! Sit quasi quod
                                            accusamus eos quod. Ab quos consequuntur eaque quo rem! Mollitia
                                            reiciendis porro quo magni incidunt dolore amet atque facilis ipsum
                                            deleniti rem!
                                        </p>
                                    </Tab>
                                    <Tab eventKey="aditional" title="Aditional Info">
                                        <p>
                                            Ipsum molestiae natus adipisci modi eligendi? Debitis amet quae unde
                                            commodi aspernatur enim, consectetur. Cumque deleniti temporibus
                                            ipsam atque a dolores quisquam quisquam adipisci possimus
                                            laboriosam. Quibusdam facilis doloribus debitis! Sit quasi quod
                                            accusamus eos quod. Ab quos consequuntur eaque quo rem! Mollitia
                                            reiciendis porro quo magni incidunt dolore amet atque facilis ipsum
                                            deleniti rem!
                                        </p>
                                    </Tab>
                                    <Tab eventKey="policy" title="Policy">
                                        <p>
                                            Ipsum molestiae natus adipisci modi eligendi? Debitis amet quae unde
                                            commodi aspernatur enim, consectetur. Cumque deleniti temporibus
                                            ipsam atque a dolores quisquam quisquam adipisci possimus
                                            laboriosam. Quibusdam facilis doloribus debitis! Sit quasi quod
                                            accusamus eos quod. Ab quos consequuntur eaque quo rem! Mollitia
                                            reiciendis porro quo magni incidunt dolore amet atque facilis ipsum
                                            deleniti rem!
                                        </p>
                                    </Tab>
                                </Tabs>
                            </Modal.Body>
                            <Modal.Footer>
                                <Col>
                                    <Form>
                                        <Form.Group className="mb-3" controlId={this.state.popupId}>
                                            <Form.Check name="isCurrentPath" type="checkbox"
                                                value={this.state.checkboxValues[this.state.popupId]}
                                                checked={this.state.checkboxValues[this.state.popupId] || false}
                                                label="Set As Current Path" onChange={(e) => { this.onCurrentPathSet(e) }} />
                                        </Form.Group>
                                    </Form>
                                </Col>
                                <Col md={{ span: 3, offset: 3 }}>
                                    <Button variant="primary" onClick={(e) => { this.handleClose(e) }}>
                                        Submit
                                    </Button>
                                </Col>
                            </Modal.Footer>
                        </Modal>
                    </div>
                </div>
            </>
        );
    }
}

export default TabArea1