import React from "react";
import "react-pro-sidebar/dist/css/styles.css";
import "./ListArea.css";
import Lego from '../D3Tree/Lego';

class ListArea extends React.Component {
    drag = (e) => {
        e.dataTransfer.setData("text", e.target.id);
    }

    getMenuItems() {
        const menuList = [];
        this.props.array.map((item, index) => {
            return menuList[index] = <div className="trans4m" key={index} id={item} onDragStart={this.drag} draggable={true}>
                <Lego nodeDatum={item} bgColor="blue" top="161px" left="-74px"></Lego>
            </div>
        })
        return menuList;
    }

    render() {
        return (
            <div className="h-100 draggableArea">
                <div>{this.getMenuItems()}</div>
            </div>
        )
    }
}

export default ListArea
