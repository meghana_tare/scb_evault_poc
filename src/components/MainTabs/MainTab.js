import React from 'react'
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';
import SearchClient from '../ClientManagement/SearchClient/SearchClient';
import SearchTree from '../TreeManagement/SearchTree/SearchTree';

class MainTab extends React.Component {
    render() {
        return (
            <Tabs>
                <TabList>
                    <Tab>Client Management</Tab>
                    <Tab>Tree Management</Tab>
                </TabList>

                <TabPanel>
                    <SearchClient></SearchClient>
                </TabPanel>
                <TabPanel>
                    <SearchTree></SearchTree>
                </TabPanel>
            </Tabs>
        )
    }
}

export default MainTab
