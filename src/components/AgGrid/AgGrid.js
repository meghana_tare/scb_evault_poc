import React from 'react';
import { AgGridReact, AgGridColumn } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';
import './AgGrid.css';
import LinkComp from "../LinkComp/LinkComp";


class AgGrid extends React.Component {
    state = {
        rowData: [
            { ClientName: "Toyota", ClientId: "Celica", ContactNo: 35000 },
            { ClientName: "Ford", ClientId: "Mondeo", ContactNo: 32000 },
            { ClientName: "Porsche", ClientId: "Boxter", ContactNo: 72000 },
            { ClientName: "Toyota", ClientId: "Celica", ContactNo: 35000 },
            { ClientName: "Ford", ClientId: "Mondeo", ContactNo: 32000 },
            { ClientName: "Porsche", ClientId: "Boxter", ContactNo: 72000 },
            { ClientName: "Toyota", ClientId: "Celica", ContactNo: 35000 },
            { ClientName: "Ford", ClientId: "Mondeo", ContactNo: 32000 },
            { ClientName: "Porsche", ClientId: "Boxter", ContactNo: 72000 }
        ]
    }

    render() {

        return (
            <>
                <div className="ag-theme-balham grid" >
                    <AgGridReact
                        rowData={this.state.rowData}>
                        <AgGridColumn headerName="Client Name" field="ClientName" sortable={true} filter={true}></AgGridColumn>
                        <AgGridColumn headerName="Client Id" field="ClientId" sortable={true} filter={true}></AgGridColumn>
                        <AgGridColumn headerName="Contact No." field="ContactNo" sortable={true} filter={true}></AgGridColumn>
                        <AgGridColumn headerName="Client Name" field="ClientName" sortable={true} filter={true}></AgGridColumn>
                        <AgGridColumn headerName="Client Id" field="ClientId" sortable={true} filter={true}></AgGridColumn>
                        <AgGridColumn headerName="Add Tree" width={120} field="AddTree" sortable={true} filter={true}
                            cellRendererFramework={LinkComp}></AgGridColumn>
                        <AgGridColumn headerName="Edit Client" width={120} field="EditClient" sortable={true} filter={true}
                            cellRendererFramework={LinkComp}></AgGridColumn>
                        <AgGridColumn headerName="View Client" width={120} field="ViewClient" sortable={true} filter={true}
                            cellRendererFramework={LinkComp}></AgGridColumn>
                        <AgGridColumn headerName="Delete Client" width={120} field="DeleteClient" sortable={true} filter={true}
                            cellRendererFramework={LinkComp}></AgGridColumn>
                    </AgGridReact >
                </div >
            </>
        )
    }
}

export default AgGrid
