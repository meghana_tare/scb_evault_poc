import React from 'react'
import { Button, Row, Col } from 'react-bootstrap';
import './SearchClient.css';
import { VscRefresh, VscSearch } from "react-icons/vsc";
import AgGrid from '../../AgGrid/AgGrid'

class SearchClient extends React.Component {
    state = {
        searchText: ''
    }

    onSearchClick(e) {
        debugger
        this.setState({ searchText: '' })
    }

    onResetClick(e) {
        debugger
        this.setState({ searchText: '' })
    }

    render() {
        return (
            <div className="divStyle">
                <Row>
                    <Col>
                        <h2>Search Client</h2>
                    </Col>
                    <Col>
                        <Button className="addbtn" variant="outline-primary" onClick={event => window.location.href = '/createClient'}>
                            + Add Client</Button>
                    </Col>
                </Row>
                <Row>
                    <Col className="searchStyle" md={{ span: 3, offset: 8 }}>
                        <input
                            className={"searchStyle h-100"}
                            key="random1"
                            value={this.state.searchText}
                            placeholder={"Search Client"}
                            onChange={(e) => this.setState({ searchText: e.target.value })}
                        />
                    </Col>
                    <Button onClick={(e) => { this.onSearchClick(e) }}><VscSearch /></Button>
                    <Button className="resetBtn" onClick={(e) => { this.onResetClick(e) }}><VscRefresh /></Button>
                </Row>
                <br />
                <Row>
                    <Col md="12">
                        <AgGrid></AgGrid>
                    </Col>
                </Row>
            </div >
        )
    }
}

export default SearchClient
