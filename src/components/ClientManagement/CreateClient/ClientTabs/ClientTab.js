import React from 'react'
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import { Form, Button, Col, Row } from 'react-bootstrap';
import {Redirect} from 'react-router-dom';
import './ClientTab.css';

class ClientTab extends React.Component {
    state = {
        name : ''
    }

    onSubmitClick(){
        debugger
        document.location.href = '/'
    }
    
    render() {
        return (
            <Tabs className="tabStyle">
                <TabList>
                    <Tab>Basic Info</Tab>
                    <Tab>Accounts</Tab>
                    <Tab>Summary</Tab>
                </TabList>

                <TabPanel>
                <Form>
                    <Form.Group className="mb-3" controlId="formGridAddress1">
                        <Form.Label>Full Name</Form.Label>
                        <Form.Control placeholder="Enter full name" />
                    </Form.Group>


                    <Row className="mb-3">
                        <Form.Group as={Col} controlId="formGridEmail">
                        <Form.Label>Email</Form.Label>
                        <Form.Control type="email" placeholder="Enter email" />
                        </Form.Group>

                        <Form.Group as={Col} controlId="formGridPassword">
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" placeholder="Password" />
                        </Form.Group>
                    </Row>

                    <Form.Group className="mb-3" controlId="formGridAddress1">
                        <Form.Label>Address</Form.Label>
                        <Form.Control placeholder="1234 Main St" />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="formGridAddress2">
                        <Form.Label>Address 2</Form.Label>
                        <Form.Control placeholder="Apartment, studio, or floor" />
                    </Form.Group>

                    <Row className="mb-3">
                        <Form.Group as={Col} controlId="formGridCity">
                        <Form.Label>City</Form.Label>
                        <Form.Control />
                        </Form.Group>

                        <Form.Group as={Col} controlId="formGridZip">
                        <Form.Label>Zip</Form.Label>
                        <Form.Control />
                        </Form.Group>
                    </Row>

                   <Row className="mb-3">
                       <Col md={2}>
                        <Form.Group as={Col} id="formGridCheckbox">
                            <Form.Check type="checkbox" label="Check me out" />
                        </Form.Group> 
                       </Col>
                       <Col md={3}>
                        <Button as={Col} className="btnStyl2" variant="primary" onClick={(e) => document.location.href = '/'}>
                                Save</Button>
                       </Col>
                    </Row>
                    </Form>
                </TabPanel>
                <TabPanel>
                    Accounts Details.....
                </TabPanel>
                <TabPanel>
                    Summary....
                </TabPanel>
            </Tabs >
        )
    }
}

export default ClientTab
