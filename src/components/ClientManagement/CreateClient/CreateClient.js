import React from 'react';
import { Button, Col, Row } from 'react-bootstrap';
import { VscArrowLeft } from "react-icons/vsc";
import ReactTooltip from "react-tooltip";
import ClientTab from './ClientTabs/ClientTab';

class CreateClient extends React.Component {
    onBackClick(e) {
        this.props.history.push('./');
    }

    render() {
        return (
            <div className="divStyle">
                <Row>
                    <Col md="1">
                        <Button variant="outline-primary" data-tip="Back" onClick={(e) => { this.onBackClick(e) }}>
                            <VscArrowLeft />
                            <ReactTooltip type="light" />
                        </Button>
                    </Col>
                    <Col md="4">
                        <h2>Add Client</h2>
                    </Col>
                </Row>
                <ClientTab></ClientTab>
            </div>
        )
    }
}

export default CreateClient
