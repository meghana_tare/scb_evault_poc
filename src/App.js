import React from 'react';
import "bootstrap/dist/css/bootstrap.min.css";
import MainTab from './components/MainTabs/MainTab';
import CreateClient from './components/ClientManagement/CreateClient/CreateClient';
import CreateTree from './components/TreeManagement/CreateTree/CreateTree';
import SearchTree from './components/TreeManagement/SearchTree/SearchTree';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

class App extends React.Component {

  render() {
    return (
      <Router>
        <Switch>
          <Route path="/" exact component={MainTab}></Route>
          {/* <Route path="/searchClient" exact component={MainTab}></Route> */}
          <Route path="/createClient" exact component={CreateClient}></Route>
          <Route path="/searchTree" exact component={SearchTree}></Route>
          <Route path="/createTree" exact component={CreateTree}></Route>
        </Switch>
      </Router>
    );
  }
}

export default App;
